from pathlib import Path

FLASK_SETTINGS = {
    'host': '127.0.0.1',
    'port': 5000,
    'debug': True
}

DATA_PATH = Path('data')
DB_NAME = 'tree'

FLASK_SECRET_KEY = ''

try:
    from local_settings import *
except ModuleNotFoundError:
    pass
