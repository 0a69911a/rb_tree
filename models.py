from flask_sqlalchemy import SQLAlchemy

from app import app

db = SQLAlchemy(app)


class Tree(db.Model):
    id = db.Column(db.String, primary_key=True)
    path_to_pickled = db.Column(db.String)
