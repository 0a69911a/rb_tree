from flask import Flask

from settings import FLASK_SETTINGS, FLASK_SECRET_KEY
from views import rb_tree

app = Flask(__name__, static_folder='templates/js')
app.register_blueprint(rb_tree)


if __name__ == '__main__':
    app.secret_key = FLASK_SECRET_KEY
    app.run(**FLASK_SETTINGS)
