from collections import deque
from dataclasses import dataclass, field
from typing import Optional, Any, List
import uuid

from algo.constants import RED, BLACK


@dataclass
class Node:
    id: uuid.UUID = field(init=False, default_factory=uuid.uuid4)
    value: Any = None
    left: Optional['Node'] = None
    right: Optional['Node'] = None
    parent: Optional['Node'] = None
    color: bool = False
    lay: int = -1

    @property
    def uncle(self):
        if self.parent and self.parent.parent:
            father = self.parent
            grandfather = father.parent
            if grandfather:
                if grandfather.right == father:
                    return grandfather.left
                elif grandfather.left == father:
                    return grandfather.right
        return None

    @property
    def brother(self):
        if self.parent is None:
            return None
        if self.parent.left is self:
            return self.parent.right
        else:
            return self.parent.left

    @property
    def black_height(self):
        node = self
        counter = self.color == BLACK
        while node:
            if node.color == BLACK:
                counter += 1
            node = node.right
        return counter

    @property
    def left_child(self) -> 'Node':
        node = self
        while node.left:
            node = node.left
        return node

    @property
    def right_child(self) -> 'Node':
        node = self
        while node.right:
            node = node.right
        return node

    def children_as_list(self):
        left = [] if self.left is None else self.left.children_as_list()
        result = left + [self]
        right = [] if self.right is None else self.right.children_as_list()
        result += right
        return result


class BaseRBTree:
    root: Optional['Node'] = None
    size: int = 0

    def _rotate_left(self, node: Optional[Node]):
        """Rotate node to left"""
        if node is None:
            return
        right = node.right
        parent = node.parent

        if right:
            node.right = right.left
            if right.left:
                right.left.parent = node
        right.parent = parent
        if parent:
            if node is parent.left:
                node.parent.left = right
            else:
                node.parent.right = right
        else:
            self.root = right

        right.left = node
        if node:
            node.parent = right

    def _rotate_right(self, node: Optional[Node]):
        """Rotate node to right"""
        if node is None:
            return
        left = node.left
        parent = node.parent
        if left:
            node.left = left.right
            if left.right:
                left.right.parent = node
        left.parent = parent
        if parent:
            if node is parent.right:
                node.parent.right = left
            else:
                node.parent.left = left
        else:
            self.root = left

        left.right = node
        if node:
            node.parent = left

    def insert(self, value):
        self.size += 1
        new_node = Node(left=None, right=None, value=value, color=RED)
        if self.root is None:
            self.root = new_node
        else:
            next_node = self.root
            parent = None
            while next_node is not None:
                parent = next_node
                print(next_node.value)
                next_node = next_node.right if new_node.value > next_node.value else next_node.left
            new_node.parent = parent
            if parent.value < new_node.value:
                parent.right = new_node
            else:
                parent.left = new_node
            self._fix_insert(new_node)

    def _fix_insert(self, node: Node):
        if node is self.root:
            node.color = BLACK
            return
        while node is not self.root and node.parent.color == RED:
            uncle = node.uncle
            parent = node.parent
            grandfather = parent.parent
            if grandfather is None:
                break
            if parent is grandfather.left:
                if uncle and uncle.color == RED:
                    parent.color = BLACK
                    uncle.color = BLACK
                    grandfather.color = RED
                    node = grandfather
                elif uncle is None or uncle.color == BLACK:
                    if node is parent.right:
                        node = parent
                        self._rotate_left(node)
                    node.parent.color = BLACK
                    node.parent.parent.color = RED
                    self._rotate_right(node.parent.parent)
            else:
                if uncle and uncle.color == RED:
                    parent.color = BLACK
                    uncle.color = BLACK
                    grandfather.color = RED
                    node = grandfather
                elif uncle is None or uncle.color == BLACK:
                    if node is parent.left:
                        node = parent
                        self._rotate_right(node)
                    node.parent.color = BLACK
                    node.parent.parent.color = RED
                    self._rotate_left(node.parent.parent)
        self.root.color = BLACK

    def find_node(self, value) -> Optional[Node]:
        node = self.root
        while node.value != value:
            node = node.right if value > node.value else node.left
            if node is None:
                return
        return node

    def _find_next_node(self, node: Node) -> Node:
        next_node = node.right
        while next_node.left is not None:
            next_node = next_node.left
        return next_node

    def remove(self, value):
        self.size -= 1
        node = self.find_node(value)
        if node is None:
            raise KeyError('Key not exist')
        else:
            self.delete_node(node)

    def delete_node(self, node):
        if node.left is None or node.right is None:
            next_node = node
        else:
            next_node = node.right
            while next_node.left is not None:
                next_node = next_node.left
        grand_child = next_node.left if next_node.left is not None else next_node.right
        grand_child.parent = next_node.parent
        if next_node.parent is not None:
            if next_node is next_node.parent.left:
                next_node.parent.left = grand_child
            else:
                next_node.parent.right = grand_child
        else:
            self.root = grand_child
        if next_node is not node:
            node.value = next_node.value
        if next_node.color == BLACK:
            self._fix_remove(grand_child)
        del next_node
        self.size -= 1

    def _fix_remove(self, node: Node):
        while node.color == BLACK and node is not self.root:
            brother = node.brother
            if node is node.parent.left:
                if brother.color == RED:
                    brother.color = BLACK
                    node.parent.color = RED
                    self._rotate_left(node.parent)
                    brother = node.parent.right
                if brother.left.color == BLACK and brother.right.color == BLACK:
                    brother.color = RED
                    node = node.parent
                else:
                    if brother.right.color == BLACK:
                        brother.left.color = BLACK
                        brother.color = RED
                        self._rotate_right(brother)
                        brother = node.parent.right
                    brother.color = node.parent.color
                    node.parent.color = BLACK
                    brother.right.color = BLACK
                    self._rotate_left(node.parent)
                    node = self.root
            else:
                if brother.color == RED:
                    brother.color = BLACK
                    node.parent.color = RED
                    self._rotate_right(node.parent)
                    brother = node.parent.left
                if brother.left.color == BLACK and brother.right.color == BLACK:
                    brother.color = RED
                    node = node.parent
                else:
                    if brother.left.color == BLACK:
                        brother.right.color = BLACK
                        brother.color = RED
                        self._rotate_left(brother)
                        brother = node.parent.left
                    brother.color = node.parent.color
                    node.parent.color = BLACK
                    brother.left.color = BLACK
                    self._rotate_right(node.parent)
                    node = self.root
        self.root.color = BLACK

    def reverse_colors(self, node=None):
        node = node or self.root
        node_stack = deque([node])
        while any(node_stack):
            node.color = not node.color
            if node.right:
                node_stack.append(node.right)
            if node.left:
                node_stack.append(node.left)
            node = node_stack.popleft()

    @property
    def black_height(self):
        node = self.root
        count = 0
        while node:
            if node.color == BLACK:
                count += 1
            node = node.left
        return count + 1


class RBTree(BaseRBTree):
    """
    https://www.cs.cmu.edu/~guyb/papers/BFS16.pdf
    """

    @classmethod
    def join(cls, self: 'BaseRBTree', other: 'BaseRBTree', key: Any) -> Optional['BaseRBTree']:
        bh_self = self.black_height
        bh_other = other.black_height
        if bh_other > bh_self:
            node = cls._join_to_right(self.root, other.root, key)
            tree = cls()
            tree.root = node
            if node.color == RED and node.right.color == RED:
                tree.root.color = BLACK
            return tree
        elif bh_other < bh_self:
            node = cls._join_to_left(self.root, other.root, key)
            tree = cls()
            tree.root = node
            if node.color == RED and node.left.color == RED:
                tree.root.color = BLACK
            return tree
        else:
            tree = cls()
            tree.insert(key)
            if self.root.value < other.root.value:
                tree.root.left = self.root
                tree.root.right = other.root
            else:
                tree.root.right = other.root
                tree.root.left = self.root
            self.root.parent = tree.root
            other.root.parent = tree.root
            if self.root.color == BLACK and other.root.color == BLACK:
                tree.root.color = RED
            return tree

    @classmethod
    def _join_to_right(cls, node_left: Node, node_right: Node, key: Any) -> Node:
        if node_left.black_height == node_right.black_height:
            node = Node(key, color=RED, left=node_left, right=node_right)
            node_left.parent = node
            node_right.parent = node
        else:
            node = Node(value=node_left.value,
                        color=RED,
                        left=node_left.left,
                        right=cls._join_to_right(node_left.right, node_right, key))
            if node_left.color == BLACK and node.right and node.right.right\
                and node.right.color == RED == node.right.right.color:
                node.right.right.color = BLACK
                cls._rotate_left(node)
            return node

    @classmethod
    def _join_to_left(cls, node_left: Node, node_right: Node, key: Any) -> Node:
        if node_left.black_height == node_right.black_height:
            node = Node(key, color=RED, left=node_left, right=node_right)
            node_left.parent = node
            node_right.parent = node
        else:
            node = Node(value=node_right.value,
                        color=RED,
                        left=cls._join_to_left(node_left, node_right.left, key),
                        right=node_right.right)
            if node_right.color == BLACK and node.left and node.left.left\
                and node.left.color == RED == node.left.left.color:
                node.left.left.color = BLACK
                cls._rotate_right(node)
            return node

