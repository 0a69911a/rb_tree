import pickle
import shelve
import uuid
from itertools import groupby
from typing import Iterable, Tuple, List, Optional

from algo.constants import COLORS_MAPPING
from algo.tree import RBTree, Node
from settings import DB_NAME

Point = Tuple[float, float]
HORIZONTAL_OFFSET = 10
VERTICAL_OFFSET = 20
CIRCLE_DIAMETER = 40
START_WIDTH = 600


def node_to_json(node: Node, position: Point):
    return {
        'id': node.id.hex,
        'parent_id': getattr(getattr(node.parent, 'id', None), 'hex', None),
        'value': node.value,
        'position': position,
        'color': COLORS_MAPPING[node.color]
    }


class ViewTree(RBTree):
    id: Optional[str] = None
    version_number = 0

    def __init__(self):
        super(ViewTree, self).__init__()
        self.id = uuid.uuid4().hex

    def insert(self, value):
        self.version_number += 1
        result = super(ViewTree, self).insert(value)
        self.save()
        print(list(shelve.open(DB_NAME)[self.id]))
        return result

    def remove(self, value):
        self.version_number += 1
        result = super(ViewTree, self).remove(value)
        self.save()
        return result

    def list(self) -> List[Node]:
        return self.root.children_as_list()

    def set_lay(self, node=None):
        if node is None:
            node = self.root
            node.lay = 0
        else:
            node.lay = node.parent.lay + 1
        if node.left:
            self.set_lay(node.left)
        if node.right:
            self.set_lay(node.right)

    def represent(self) -> dict:
        self.set_lay()
        all_nodes = self.list()
        # width = len(all_nodes) * (HORIZONTAL_OFFSET + CIRCLE_DIAMETER)
        result = []
        for number, node in enumerate(all_nodes):
            x = HORIZONTAL_OFFSET + CIRCLE_DIAMETER / 2 + number * (HORIZONTAL_OFFSET + CIRCLE_DIAMETER)
            y = VERTICAL_OFFSET + node.lay * (VERTICAL_OFFSET + CIRCLE_DIAMETER)
            result.append(node_to_json(node, (x, y)))
        return {
            'h_offset': HORIZONTAL_OFFSET,
            'v_offset': VERTICAL_OFFSET,
            'circle_diameter': CIRCLE_DIAMETER,
            'nodes': result,
            'id': self.id,
            'version': self.version_number
        }

    def find_path(self, value) -> List[str]:
        node = self.root
        result = []
        while node.value != value:
            if value > node.value:
                node = node.right
                result.append('right')
            else:
                node = node.left
                result.append('left')
            if node is None:
                return []
        return result

    def get_version(self, version_number: int):
        with shelve.open(DB_NAME) as db:
            return pickle.loads(db[self.id][version_number])

    def save(self):
        with shelve.open(DB_NAME) as db:
            db.setdefault(self.id, [])
            db[self.id] += [pickle.dumps(self)]
