import json
import shelve
import uuid
from typing import Optional

from flask import render_template, Blueprint, request, jsonify
from flask.views import MethodView

from algo.tree_view import ViewTree
import pickle

from settings import DATA_PATH, DB_NAME


def load_tree(tree_id):
    print(tree_id, DB_NAME)
    with shelve.open(DB_NAME) as db:
        return pickle.loads(db[tree_id][-1])


rb_tree = Blueprint('RBTree', __name__, template_folder='templates')


@rb_tree.route('/')
def homepage():
    return render_template('tree.html', tree=json.dumps({}), post_url='/tree/methods')


@rb_tree.route('/tree/<string:tree_id>')
def get_tree(tree_id):
    try:
        tree = load_tree(tree_id)
        print(json.dumps(tree.represent()))
        return render_template('tree.html', tree=json.dumps(tree.represent()), name=tree_id, post_url='/tree/methods')
    except KeyError:
        return render_template('error404.html', name=tree_id)


class UpdateTreeView(MethodView):
    def post(self):
        data = request.json
        print(data)
        tree: Optional[ViewTree] = None
        if data['tree_id'] is None:
            tree = ViewTree()
        else:
            try:
                tree = load_tree(data['tree_id'])
            except (TypeError, FileNotFoundError) as e:
                if data['type'] != 'save':
                    print(e)
                    return jsonify(error=True, msg=str(e))
            except KeyError as e:
                tree = ViewTree()
                print(e)
        if data['type'] in ('find', 'insert', 'remove'):
            data['value'] = int(data['value'])
        if data['type'] == 'find':
            return jsonify(tree.find_path(data['value']))
        elif data['type'] == 'insert':
            tree.insert(value=data['value'])
        elif data['type'] == 'remove':
            tree.remove(value=data['value'])
        else:
            raise TypeError
        print(tree.represent())
        return jsonify(tree.represent())


rb_tree.add_url_rule('/tree/methods', view_func=UpdateTreeView.as_view('post_tree'))
