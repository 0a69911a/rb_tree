function request(url, body) {
    return fetch(url, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body),
    }).then(data => data.json());
}



function getNode(tree_id, url) {
    const promise = request(url, {'node_id': tree_id});
    promise.then((data) => {
        this.data = data;
    });
}

function draw(from_request, tree_id = null, url = null, tree=null) {
    if (from_request) {
        getNode(tree_id, url);
        _draw(this.data);
    } else {
        _draw(tree);
    }
}

function next(arr, pred, def=null) {
    for (let val of arr) {
        if (pred(val)) {
            return val;
        }
    }
    return def;
}

function drawLine(ctx, nodeFrom, nodeTo) {
    ctx.fillStyle = 'black';
    ctx.beginPath();
    let start_point = [ ...nodeFrom['position'] ];
    let end_point = [ ...nodeTo['position'] ];
    start_point[1] += this.circleDiameter / 2;
    end_point[1] -= this.circleDiameter / 2;
    ctx.moveTo(...start_point);
    ctx.lineTo(...end_point);
    ctx.stroke();
}

function _draw(tree) {
    this.data = tree;
    this.circleDiameter = tree['circle_diameter'];
    tree = tree['nodes'];
    let canvas = document.getElementById('canvas');
    let ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    this.tree = tree;
    for (let node of tree) {
        ctx.fillStyle = node['color'];
        ctx.beginPath();
        ctx.arc(...node['position'], this.circleDiameter / 2, 0, Math.PI * 2);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
        ctx.fillStyle = 'white';
        ctx.font = "20px Arial";
        ctx.textBaseline = 'middle';
        ctx.textAlign = 'center';
        ctx.fillText(node['value'], node['position'][0], node['position'][1], this.circleDiameter * 0.75);
        if (node['parent_id'] !== null) {
            let parent = next(tree, _node => _node['id'] === node['parent_id']);
            drawLine(ctx, parent, node);
        } else {
            this.root = node;
        }
    }
}

function drawPath(list) {
    this._draw(this.data);
    let canvas = document.getElementById('canvas');
    let ctx = canvas.getContext('2d');
    let node = this.root;
    const oldStrokeStyle = ctx.strokeStyle;
    const oldLineWidth = ctx.lineWidth;
    ctx.strokeStyle = '#dfb51b';  // golden
    ctx.lineWidth = 5;
    ctx.beginPath();
    ctx.arc(...node.position, this.circleDiameter / 2, 0, Math.PI * 2);
    ctx.stroke();
    let prevNode = node;
    for (let value of list) {
        if (value === 'right') {
            node = next(this.tree, _node => (_node['parent_id'] === node['id'] && _node['value'] > node['value']));
        } else {
            node = next(this.tree, _node => (_node['parent_id'] === node['id'] && _node['value'] < node['value']));
        }
        ctx.beginPath();
        ctx.arc(...node.position, this.circleDiameter / 2, 0, Math.PI * 2);
        ctx.stroke();
        drawLine(ctx, prevNode, node);
        prevNode = node;
    }
    ctx.lineWidth = oldLineWidth;
    ctx.strokeStyle = oldStrokeStyle;
}

function postForm(url, type, form) {
    const tree_id = location.pathname === '/' ? null : location.pathname.substring('/tree/'.length);
    request(url,{'type': type, 'tree_id': tree_id, 'value': form.previousElementSibling.firstElementChild.value}).then(
        data => {
            if (type !== 'find') {
                draw(false, null, null, data);
                if (location.pathname === '/') {
                    location.pathname += `tree/${data.id}`
                }
            } else {
                drawPath(data);
            }
        }
    );
}
